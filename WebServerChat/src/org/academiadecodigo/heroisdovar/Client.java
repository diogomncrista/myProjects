package org.academiadecodigo.heroisdovar;

import java.io.*;
import java.net.Socket;

public class Client {

    private String name;
    private Socket socket;
    private BufferedReader inputBufferedReader;
    private BufferedWriter outputBufferedWriter;

    public static void main(String[] args) {
        Client client = new Client("localhost", 8000);
        client.nameClient();
    }

    public Client(String serverAddress, int serverPort){
        try {
            this.socket = new Socket(serverAddress, serverPort);
            this.inputBufferedReader = new BufferedReader(new InputStreamReader(System.in));
            this.outputBufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.getStackTrace();
        }
        Thread receiveMsg = new Thread (new Receive(socket));
        receiveMsg.start();
    }

    public void nameClient(){
        System.out.println("What is your nickname?");
        try {
            this.name =  inputBufferedReader.readLine();
        } catch (IOException e){
            e.getStackTrace();
        }
        System.out.println("Welcome " + name);
        run();
    }

    private void run() {
        while (socket.isConnected()) {
            try {


                String line = inputBufferedReader.readLine();

                if(line.equals("/quit")){
                    socket.close();
                }

                // write the pretended message to the output buffer
                outputBufferedWriter.write(name + ": " + line);
                outputBufferedWriter.newLine();
                outputBufferedWriter.flush();

            } catch (IOException ex) {

                System.out.println("Closing client...");
                break;

            }
        }
    }

    private static class Receive implements Runnable {
        private BufferedReader inputBufferedReader;
        private BufferedWriter outputBufferedWriter;
        private final Socket socket;

        public Receive(Socket socket) {
            this.socket = socket;
            try {
                this.inputBufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                this.outputBufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));
            } catch (IOException e) {
                e.getStackTrace();
            }
        }

        @Override
        public void run() {

            while (socket.isConnected()){
                try {

                    String line = inputBufferedReader.readLine();

                    // write the pretended message to the output buffer
                    outputBufferedWriter.write(line);
                    outputBufferedWriter.newLine();
                    outputBufferedWriter.flush();

                } catch (IOException ex) {

                    ex.getStackTrace();
                    break;
                }
            }
        }
    }
}
