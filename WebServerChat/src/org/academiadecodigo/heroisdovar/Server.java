package org.academiadecodigo.heroisdovar;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private final List<ServerWorker> list;
    private final int port;

    public static void main(String[] args) {
        Server server = new Server(8000);
        server.acceptConnections();
    }

    public Server(int port) {
        this.list = new ArrayList<>();
        this.port = port;
    }

    private void acceptConnections() {
        try (ServerSocket socket = new ServerSocket(port)) {

            while (!socket.isClosed()) {
                Socket clientsocket = socket.accept();
                System.out.println("New connection");
                ServerWorker sw = new ServerWorker(clientsocket, this);
                Thread threadSw = new Thread(sw);
                list.add(sw);
                threadSw.start();
            }

        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public void sendAll(String line) {
        for (ServerWorker sw : list) {
            try {
                sw.bWriter.write(line);
                sw.bWriter.newLine();
                sw.bWriter.flush();
            } catch (IOException e) {
                e.getStackTrace();
            }
        }
    }

    private static class ServerWorker implements Runnable {

        private final BufferedReader bReader;
        private final BufferedWriter bWriter;
        private final Server server;

        public ServerWorker(Socket clientSocket, Server server) {
            this.server = server;
            try {
                bReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                bWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void run() {

            try {
                String line = bReader.readLine();
                while (line != null) {
                    server.sendAll(line);
                    line = bReader.readLine();
                }

            } catch (IOException e) {
                e.getStackTrace();
            }
        }
    }
}
