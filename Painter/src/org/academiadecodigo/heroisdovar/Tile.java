package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Tile extends Rectangle{

    private boolean isFilled;
    private Color currentColor;

    //Constructor
    public Tile(double x, double y, double cellsize, double cellsize1) {
        super(x,y,cellsize,cellsize1);
        this.isFilled = false;
        this.currentColor = Color.BLACK;
    }

    //getter and setter
    public boolean isFilled() {
        return isFilled;
    }

    public Color getCurrentColor() {
        return currentColor;
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        currentColor = color;
    }

    public void setFilled(boolean filled) {
        isFilled = filled;
    }

}
