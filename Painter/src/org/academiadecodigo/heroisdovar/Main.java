package org.academiadecodigo.heroisdovar;

public class Main {
    public static void main(String[] args) {
        Grid grid = new Grid(20,30);
        Cursor cursor = new Cursor(grid);

        grid.makeGrid();

        new Handler(cursor);
    }
}
