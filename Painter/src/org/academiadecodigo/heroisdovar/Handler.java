package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.IOException;

public class Handler implements KeyboardHandler {

    private final Keyboard keyboard;
    private final Cursor cursorDrawer;
    private final KeyboardEvent[] events;
    private Color color;
    private boolean isShiftPressed;
    private boolean isPaiting;
    private boolean is0Pressed;
    private boolean is1Pressed;
    private boolean is2Pressed;
    private boolean isDPressed;


    public Handler(Cursor cursorDrawer){
        keyboard = new Keyboard(this);
        events = new KeyboardEvent[18];
        createKeyboardEvents();
        this.cursorDrawer = cursorDrawer;
        this.isShiftPressed = false;
        this.isDPressed = false;
        this.isPaiting = false;
        this.is1Pressed = false;
        this.is0Pressed = false;
        this.is2Pressed = false;
        this.color = Color.BLACK;
    }

    public void createKeyboardEvents() {

        for (int i = 0; i < events.length; i++){
            events[i] = new KeyboardEvent();
        }

        //key pressed
        events[0].setKey(KeyboardEvent.KEY_UP);
        events[1].setKey(KeyboardEvent.KEY_DOWN);
        events[2].setKey(KeyboardEvent.KEY_RIGHT);
        events[3].setKey(KeyboardEvent.KEY_LEFT);
        events[4].setKey(KeyboardEvent.KEY_SPACE);
        events[5].setKey(KeyboardEvent.KEY_S);
        events[6].setKey(KeyboardEvent.KEY_L);
        events[7].setKey(KeyboardEvent.KEY_D);
        events[8].setKey(KeyboardEvent.KEY_SHIFT);
        events[9].setKey(KeyboardEvent.KEY_0);
        events[10].setKey(KeyboardEvent.KEY_1);
        events[11].setKey(KeyboardEvent.KEY_2);

        //key released
        events[12].setKey(KeyboardEvent.KEY_0);
        events[13].setKey(KeyboardEvent.KEY_1);
        events[14].setKey(KeyboardEvent.KEY_2);
        events[15].setKey(KeyboardEvent.KEY_SPACE);
        events[16].setKey(KeyboardEvent.KEY_SHIFT);
        events[17].setKey(KeyboardEvent.KEY_D);

        //listener for key pressed
        for (int i = 0; i < 12; i++){
            events[i].setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(events[i]);
        }

        //listener for key released
        for (int i = 12; i < 18; i++){
            events[i].setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
            keyboard.addEventListener(events[i]);
        }
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:
                cursorDrawer.moveUp();
                if(isPaiting){
                    cursorDrawer.getGrid().setGridColor(color);
                    cursorDrawer.paint();
                    }
                break;

            case KeyboardEvent.KEY_DOWN:
                cursorDrawer.moveDown();
                if(isPaiting){
                    cursorDrawer.getGrid().setGridColor(color);
                    cursorDrawer.paint();
                    }
                break;

            case KeyboardEvent.KEY_RIGHT:
                cursorDrawer.moveRight();
                if (isPaiting) {
                    cursorDrawer.getGrid().setGridColor(color);
                    cursorDrawer.paint();
                    }
                break;

            case KeyboardEvent.KEY_LEFT:
                cursorDrawer.moveLeft();
                if(isPaiting){
                    cursorDrawer.getGrid().setGridColor(color);
                    cursorDrawer.paint();
                    }
                break;

            case KeyboardEvent.KEY_SPACE:
                isPaiting = true;
                cursorDrawer.paint();
                break;

            case KeyboardEvent.KEY_SHIFT:
                isShiftPressed = true;
                break;

            case KeyboardEvent.KEY_S:
                if(isShiftPressed) {
                    try {
                        System.out.println("Write to load file");
                        cursorDrawer.getGrid().writeFile("resources/outputFile");
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                break;

            case KeyboardEvent.KEY_L:
                try {
                    System.out.println("Load file");
                    cursorDrawer.getGrid().loadFile("resources/outputFile");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                break;

            case KeyboardEvent.KEY_D:
                isDPressed = true;
                cursorDrawer.getGrid().eraseScreen();
                System.out.println("Erase screen");
                break;

            case KeyboardEvent.KEY_0:
                is0Pressed = true;
                color = Color.BLACK;
                break;

            case KeyboardEvent.KEY_1:
                is1Pressed = true;
                color = Color.CYAN;
                break;

            case KeyboardEvent.KEY_2:
                is2Pressed = true;
                color = Color.GREEN;
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()){
            case KeyboardEvent.KEY_SPACE:
                isPaiting = false;
                cursorDrawer.getGrid().setGridColor(color);
                cursorDrawer.paint();
                break;

            case KeyboardEvent.KEY_D:
                isDPressed = false;
                cursorDrawer.getGrid().setGridColor(color);
                break;

            case KeyboardEvent.KEY_SHIFT:
                isShiftPressed = false;
                break;

            case KeyboardEvent.KEY_0:
                cursorDrawer.getGrid().setGridColor(color);
                is0Pressed = false;
                break;

            case KeyboardEvent.KEY_1:
                cursorDrawer.getGrid().setGridColor(color);
                is1Pressed = false;
                break;

            case KeyboardEvent.KEY_2:
                cursorDrawer.getGrid().setGridColor(color);
                is2Pressed = false;
                break;
        }
    }
}

