package org.academiadecodigo.heroisdovar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cursor {

    private final Rectangle cursor;
    private final Grid grid;

    //Constructor
    public Cursor(Grid grid){
        this.grid = grid;
        this.cursor = new Rectangle(grid.getPadding(), grid.getPadding(), grid.getCellSize(), grid.getCellSize());
        cursor.setColor(Color.YELLOW);
        cursor.fill();
    }

    //getters
    public Grid getGrid() {
        return grid;
    }

    //methods
    public int xToCol(){
        return cursor.getX() / grid.getCellSize();
    }

    public int yToRow(){
        return cursor.getY() / grid.getCellSize();
    }

    public void moveRight(){
        if (cursor.getX() + grid.getCellSize() < grid.getWidth()) {
            cursor.translate(grid.getCellSize(), 0);
        }
    }

    public void moveLeft() {
        if (cursor.getX() > grid.getRows()) {
            cursor.translate(-grid.getCellSize(), 0);
        }
    }

    public void moveDown(){
        if (cursor.getY() + grid.getCellSize() < grid.getHeight()) {
            cursor.translate(0, grid.getCellSize());
        }
    }

    public void moveUp() {
        if (cursor.getY() > grid.getCols()) {
            cursor.translate(0, -grid.getCellSize());
        }
    }

    public void paint(){
        if (!grid.getGridTile()[yToRow()][xToCol()].isFilled()){
            grid.getGridTile()[yToRow()][xToCol()].fill();
            cursor.delete();
            cursor.fill();
            grid.getGridTile()[yToRow()][xToCol()].setFilled(true);
        } else {
            grid.getGridTile()[yToRow()][xToCol()].setColor(Color.BLACK);
            grid.getGridTile()[yToRow()][xToCol()].draw();
            cursor.delete();
            cursor.fill();
            grid.getGridTile()[yToRow()][xToCol()].setFilled(false);
        }
    }
}
