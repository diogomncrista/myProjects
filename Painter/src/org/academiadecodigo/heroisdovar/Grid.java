package org.academiadecodigo.heroisdovar;


import org.academiadecodigo.simplegraphics.graphics.Color;

import java.io.*;

public class Grid {

    private Tile[][] gridTile;
    private final String[][] outputFile;
    private final int cols;
    private final int rows;

    private final int padding;

    private final int cellSize;

    //Constructor
    public Grid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.padding = 10;
        this.cellSize = 40;
        this.outputFile = new String[rows][cols];
    }


    //getters
    public int getWidth() {
        return cols * cellSize + padding;
    }

    public int getHeight() {
        return rows * cellSize + padding;
    }

    public int getCellSize() {
        return cellSize;
    }

    public int getPadding() {
        return padding;
    }

    public Tile[][] getGridTile() {
        return gridTile;
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }


    //methods
    public void makeGrid() {
        gridTile = new Tile[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int x = col * cellSize + padding;
                int y = row * cellSize + padding;
                gridTile[row][col] = new Tile(x, y, cellSize, cellSize);
                gridTile[row][col].draw();
            }
        }
    }

    public void writeFile(String file) throws IOException {
        FileWriter writer = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                Color currentColor = gridTile[row][col].getCurrentColor();
                boolean filled = gridTile[row][col].isFilled();

                if (currentColor.equals(Color.BLACK) && filled) {
                    outputFile[row][col] = "9";
                } else if (currentColor.equals(Color.CYAN) && filled) {
                    outputFile[row][col] = "1";
                } else if (currentColor.equals(Color.GREEN) && filled) {
                    outputFile[row][col] = "2";
                } else {
                    outputFile[row][col] = "0";
                }

                bufferedWriter.write(outputFile[row][col]);
            }
            bufferedWriter.write("\n");
        }

        bufferedWriter.flush();
        bufferedWriter.close();
    }
    public void loadFile(String file) throws IOException{
        FileReader reader = new FileReader(file);
        BufferedReader bReader = new BufferedReader(reader);

        String[] lineArray;
        String line;

        for (int i = 0; i<rows;i++){
            line = bReader.readLine();
            lineArray = line.split("");
            for (int j = 0;j<cols;j++){
                outputFile[i][j]= lineArray[j];
            }
        }

        for (int i = 0; i<rows;i++){
            for (int j = 0; j < cols; j++){
                switch (outputFile[i][j]) {
                    case "9":
                        gridTile[i][j].setColor(Color.BLACK);
                        gridTile[i][j].fill();
                        gridTile[i][j].setFilled(true);
                        break;
                    case "1":
                        gridTile[i][j].setColor(Color.CYAN);
                        gridTile[i][j].fill();
                        gridTile[i][j].setFilled(true);
                        break;
                    case "2":
                        gridTile[i][j].setColor(Color.GREEN);
                        gridTile[i][j].fill();
                        gridTile[i][j].setFilled(true);
                        break;
                }
            }
        }
    }
    public void eraseScreen(){
        for(int i=0; i<rows;i++){
            for(int j = 0; j<cols;j++){

                gridTile[i][j].draw();
                gridTile[i][j].setFilled(false);
            }
        }
    }
    public void setGridColor(Color newColor){
        for(int i=0; i<rows;i++){
            for(int j = 0; j<cols;j++){

                if (!gridTile[i][j].isFilled()) {
                    gridTile[i][j].setColor(newColor);
                }
            }
        }
    }
}